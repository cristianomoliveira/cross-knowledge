"""
    - Install Redis Sever:
      sudo apt-get install redis-server
    
    - Install redis package:
      pip install redis
"""
import requests
import redis


class Cache:

    def __init__(self):
        self.REDIS = redis.Redis(host='localhost', port=6379, db=0)

    
    def save(self, req, res):
        """Function to save a request.

        Args:
            param1: The request.
            param2: The response.

        Returns:
            The Response as byte type

        """
        self.REDIS.set(req, res)
        return self.REDIS.get(req)
    
    def get(self, req):
        """Function to return request that is in cache.

        Args:
            param1: The request.

        Returns:
            The Response as string

        """
        return print(str(self.REDIS.get(req),'utf-8'))